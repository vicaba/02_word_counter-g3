<?php

namespace G3Counter\Filter\ConcreteFilter;

use G3Counter\Filter\Filter;
use G3Counter\Models\Word;

class WordLengthFilter implements Filter
{

    private $wordLengthToTest;

    public function __construct($wordLength) {
        $this->wordLengthToTest = $wordLength;
    }


    public function __invoke(Word $word)
    {

        $isWordLargerThan = strlen($word->get()) > $this->wordLengthToTest;

        return $isWordLargerThan;
    }

}