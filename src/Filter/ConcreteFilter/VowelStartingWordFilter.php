<?php

namespace G3Counter\Filter\ConcreteFilter;

use G3Counter\Filter\Filter;
use G3Counter\Models\Word;

class VowelStartingWordFilter implements Filter
{


    public function __invoke(Word $word)
    {
        $vowelsToTest = ["a", "e", "i", "o", "u"];

        $wordFirstCharacter = strtolower(substr($word->get(), 0, 1));

        return in_array($wordFirstCharacter, $vowelsToTest);
    }
}