<?php

namespace G3Counter\Filter;

use G3Counter\Models\Word;

interface Filter
{
    public function __invoke(Word $word);
}