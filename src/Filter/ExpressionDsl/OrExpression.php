<?php

namespace G3Counter\Filter\ExpressionDsl;


use G3Counter\Filter\Filter;
use G3Counter\Models\Word;

class OrExpression extends Expression
{

    private $filter2;

    public function __construct(Filter $filter1, Filter $filter2)
    {
        parent::__construct($filter1);
        $this->filter2 = $filter2;
    }

    public function __invoke(Word $word)
    {
        return $this->filter->__invoke($word) || $this->filter2->__invoke($word);
    }
}