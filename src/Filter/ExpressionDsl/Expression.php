<?php

namespace G3Counter\Filter\ExpressionDsl;

use G3Counter\Filter\Filter;
use G3Counter\Models\Word;

class Expression implements ExpressiveFilter
{
    protected $filter;

    public function __construct(Filter $filter)
    {
        $this->filter = $filter;
    }

    public function __invoke(Word $word)
    {
        return $this->filter->__invoke($word);
    }

    public function orFilter(Filter $filter)
    {
        return new OrExpression($this, $filter);
    }

    public function orFilterNot(Filter $filter)
    {
        return new OrExpression($this, new NegateExpression($filter));
    }

    public function andFilter(Filter $filter)
    {
        return new AndExpression($this, $filter);
    }

    public function andFilterNot(Filter $filter)
    {
        return new AndExpression($this, new NegateExpression($filter));
    }
}