<?php

namespace G3Counter\Filter\ExpressionDsl;

use G3Counter\Filter\Filter;

interface ExpressiveFilter extends Filter
{

    public function orFilter(Filter $filter);

    public function andFilter(Filter $filter);

    public function orFilterNot(Filter $filter);

    public function andFilterNot(Filter $filter);

}