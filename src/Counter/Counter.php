<?php

namespace G3Counter\Counter;

use G3Counter\Filter\Filter;
use G3Counter\Models\Text;

interface Counter
{
    public function __invoke(Text $text, Filter $filter = null);
}