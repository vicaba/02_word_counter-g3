<?php

use G3Counter\Counter\ConcreteCounter\WordCounter;
use G3Counter\Filter\ConcreteFilter\KeywordFilter;
use G3Counter\Filter\ConcreteFilter\VowelStartingWordFilter;
use G3Counter\Filter\ConcreteFilter\WordLengthFilter;
use G3Counter\Filter\ExpressionDsl\AndExpression;
use G3Counter\Filter\ExpressionDsl\Expression;
use G3Counter\Filter\ExpressionDsl\NegateExpression;
use G3Counter\Filter\FilterChain;
use G3Counter\Models\Text;

include_once(__DIR__ . '/../vendor/autoload.php');

$inputText = <<<TEXT
Esto es un texto molón que sirve como juego de pruebas para la kata de contar palabrejas.
No me hagas un diseño de gañán ni de hiper-arquitecto. Que te veo, eh.
TEXT;


$textToTest = new Text($inputText);

$counter = new WordCounter();
$numberOfWords = $counter($textToTest);
echo "Número total de palabras $numberOfWords\n";

$vowelFilter = new VowelStartingWordFilter();

$lengthFilter = new WordLengthFilter(2);

$keyWordFilter = new KeywordFilter(["palabrejas", "gañán", "hiper-arquitecto", "que", "eh"]);

echo "Palabras que empiezan por vocal: {$counter($textToTest, $vowelFilter)}\n";

echo "Palabras de más de dos carácteres: {$counter($textToTest, $lengthFilter)}\n";

echo "Keywords: {$counter($textToTest, $keyWordFilter)}\n";

$vowelAndLengthFilter = new FilterChain([$vowelFilter, $lengthFilter]);

echo "Palabras que contienen vocal y tienen más de dos carácteres: {$counter($textToTest, $vowelAndLengthFilter)}\n";

$vowelAndKeyWordFilter = new FilterChain([$vowelFilter, $keyWordFilter]);

echo "Palabras que contienen vocal y son palabras clave: {$counter($textToTest, $vowelAndKeyWordFilter)}\n";

$allFilters = new FilterChain([$vowelAndKeyWordFilter, $lengthFilter]);

echo "Palabras que contienen vocal, son palabras clave y tiene más de dos carácteres: {$counter($textToTest, $allFilters)}\n";

echo "Expresiones con filtros\n";

$keyWordAndNotStartingWithVowel = (new Expression($keyWordFilter))->andFilterNot($vowelFilter);

echo "Palabras clave que no empiezan por vocal: {$counter($textToTest, $keyWordAndNotStartingWithVowel)}\n";

$complexFilter = (new NegateExpression($vowelFilter))->orFilter(new AndExpression($vowelFilter, $lengthFilter));

echo "Palabras que no empiezan por vocal o que empiezan por vocal pero tienen más de dos carácteres: {$counter($textToTest, $complexFilter)}\n";
