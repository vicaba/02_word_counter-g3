<?php

namespace G3Counter\Models;


class Text
{

    private $text;

    public function __construct($text)
    {
        $this->text = $text;
    }

    public function getWords()
    {
        $normalizedText = preg_replace('/[.,!?]/', "", $this->text);
        $words = explode(" ", $normalizedText);

        return array_map(function ($word) {
            return new Word($word);
        }, $words);
    }

}